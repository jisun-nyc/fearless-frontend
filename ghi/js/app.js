function convertDate(dateString) {
    const date = new Date(dateString);
    const formattedDate = `${(date.getUTCMonth() + 1).toString().padStart(2, '0')}/${date.getUTCDate().toString().padStart(2, '0')}/${date.getUTCFullYear()}`;
    return formattedDate;
}


function createCard(title, description, pictureUrl, starts, ends, place) {
    const formattedStarts = convertDate(starts);
    const formattedEnds = convertDate(ends);

    return `
    <div class="col-4">
    <div class="shadow p-3 mb-5 bg-body rounded">
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${place}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        <small class="text-body-secondary">${formattedStarts} - ${formattedEnds}</small>
        </div>
        </div>
      </div>
    </div>
    `;
  }


function handleError(e) {
    return `
    <div class="alert-primary d-flex align-items-center">
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
    <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
    </svg>
    <div>
    An error has occurred!
    </div>
    </div>
    `;
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            throw new Error('Response not ok');
        } else {
            const data = await response.json();
            // throw new Error();
            for (let conference of data.conferences) {
                const detailURL = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailURL);

                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = details.conference.starts;
                    const ends = details.conference.ends;
                    const place = details.conference.location.name;
                    const html = createCard(title, description, pictureUrl, starts, ends, place);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                }
            }
            // const conference = data.conferences[0];
            // const nameTag = document.querySelector('.card-title');
            // nameTag.innerHTML = conference.name;

        }
    } catch (e) {
        console.log(e);

        var alertList = document.querySelector('.alert')
        var alerts =  handleError(e);
        alertList.innerHTML = alerts;
        // handleError();
        // console.log('there was a problem with your request')
    }
    // const response = await fetch(url);
    // console.log(response);

    // const data = await response.json();
    // console.log(data);

    });
