window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);
    //fetching data of locations list API
    if (response.ok) {
        const data = await response.json();

        const selectTag = document.getElementById('conference');
        for (let conference of data.locations) {
            const option = document.createElement('option');
            option.value = conference.id;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }
    }

    //creating a presentation [post]
    const formTag = document.getElementById('create-presentation-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const select = document.getElementById('conference');
        const conf_id = select.options[select.selectedIndex].value;
        const presentationUrl = `http://localhost:8000/api/conferences/${conf_id}/presentations/`;

        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(presentationUrl, fetchConfig);

        if (response.ok) {
            formTag.reset();
            const newPresentation = await response.json();
        }
    });
});
