// import logo from './logo.svg';
// import './App.css';
import React from 'react';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeeConferenceForm from './AttendeeConferenceForm';
import PresentationForm from './PresentationForm';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }

  return (
    <BrowserRouter>
    <Nav />
    <div className="container">
      <Routes>
        <Route path="/">
            <Route path="" element={<App /> } />
          </Route>
          <Route path="locations">
            <Route path="new" element={<LocationForm /> } />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm /> } />
          </Route>
          <Route path="attendees">
            <Route path="new" element={<AttendeeConferenceForm /> } />
          </Route>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm /> } />
          </Route>
          <Route path="attendees">
            <Route path="" element={<AttendeesList attendees={props.attendees} /> } />
          </Route>
      </Routes>
    </div>
    </BrowserRouter>
  );
}

export default App;
